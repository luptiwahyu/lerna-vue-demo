import components from './components'

const install = function(Vue) {
  components.forEach((component) => {
    Vue.component(component.name, component)
  })
}

export default {
  version: '0.0.1',
  install,
}
