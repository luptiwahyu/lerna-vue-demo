import Button from './button'
import Hello from './hello'

const components = [
  Button,
  Hello,
]

export default components
