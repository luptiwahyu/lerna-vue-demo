const uppercase = (string) => {
  return string ? string.toUpperCase() : ''
}

export default uppercase
