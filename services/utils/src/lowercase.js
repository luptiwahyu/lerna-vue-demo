const lowercase = (string) => {
  return string ? string.toLowerCase() : ''
}

export default lowercase
