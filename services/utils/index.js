import uppercase from './src/uppercase'
import lowercase from './src/lowercase'

export default {
  uppercase,
  lowercase,
}
